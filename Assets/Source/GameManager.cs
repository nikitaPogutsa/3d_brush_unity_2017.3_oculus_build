﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
//не стал делать синглтоном
public class GameManager : MonoBehaviour
{

    public Transform target;
    public Transform colorPicker;
    
    //ColorPicker
    ColorPickerTriangle cpTriangle;
    SteamVR_ControllerManager cManager;
    readonly string cPicker = "ColorPicker";
    LineRenderer line;
    Color materialColor = new Color(85, 0, 99);

    TrailRenderer trail;

    //For ontroller input and velocity
    SteamVR_TrackedController left;
    SteamVR_TrackedController right;

    SteamVR_TrackedObject leftTrackedObject;
    SteamVR_TrackedObject rightTrackedObject;

    SteamVR_Controller.Device leftDevice;
    SteamVR_Controller.Device rightDevice;

    


    //public delegate void PickColor(Vector3 vrCursorPos);
    //public static event PickColor onColorPick;

    private void Awake()
    {
        cManager = GetComponent<SteamVR_ControllerManager>();
        left = cManager.left.GetComponent<SteamVR_TrackedController>();
        right = cManager.right.GetComponent<SteamVR_TrackedController>();
        leftTrackedObject = cManager.left.GetComponent<SteamVR_TrackedObject>();
        rightTrackedObject = cManager.right.GetComponent<SteamVR_TrackedObject>();
        leftDevice = SteamVR_Controller.Input((int)left.controllerIndex);
        rightDevice = SteamVR_Controller.Input((int)right.controllerIndex);
        line = right.GetComponent<LineRenderer>();
        trail = left.GetComponent<TrailRenderer>();
        cpTriangle = colorPicker.GetComponent<ColorPickerTriangle>();

    }
    void RayCastColorPicker()
    {
        RaycastHit hit;
        if (Physics.Raycast(right.transform.position, right.transform.forward, out hit))
        {
            if (hit.collider != null && hit.collider.tag == cPicker)
            {
                cpTriangle.ColorPick(hit.point);
                materialColor = cpTriangle.TheColor;

                SetTrailColor();
                Debug.Log(materialColor);
            }
        }
        
    }
    void SetTrailColor()
    {
        trail.material.color = materialColor;
        trail.material.SetColor("_EmissionColor", materialColor);
    }
    void Update()
    {

        if (SteamVR_Controller.Input((int)right.controllerIndex).GetPressDown(15))
        {
            DrawManager.CreateBrushInstance(right.gameObject.transform, materialColor);

        }


       
        
        if (SteamVR_Controller.Input((int)right.controllerIndex).GetPress(15))
        {

            if (rightDevice.velocity.sqrMagnitude > 0.001f)
            {

                DrawManager.Draw();
            }
        }

        //Clear 3d canvas
        if (SteamVR_Controller.Input((int)right.controllerIndex).GetPressDown(3))
        {
            
            DrawManager.Doispose();
        }


        //Trail Renderer
        if ((SteamVR_Controller.Input((int)left.controllerIndex).GetPress(14)))
        {
            trail.enabled = true;
        }
        if ((SteamVR_Controller.Input((int)left.controllerIndex).GetPressUp(14)))
        {
            trail.enabled = false;
        }
        if ((SteamVR_Controller.Input((int)left.controllerIndex).GetPressDown(14)))
        {
            trail.Clear();
        }


        //Raycast COlorPicker
        if (SteamVR_Controller.Input((int)right.controllerIndex).GetPress(2))
        {
            line.positionCount = 2;

            line.SetPositions(new Vector3[] { right.transform.position, right.transform.position + right.transform.forward * 30 });
            RayCastColorPicker();
        }
        else if (SteamVR_Controller.Input((int)right.controllerIndex).GetPressUp(2))
        {
            line.positionCount = 0;
        }

    }

}

