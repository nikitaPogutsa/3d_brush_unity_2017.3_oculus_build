﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DrawManager {
    private static GameObject brush;
    private static MeshGenerator meshGenerator;
    private static Renderer renderer;
    private static List<GameObject> strokes = new List<GameObject>();

    public static void CreateBrushInstance(Transform controller, Color brushColor)
    {
        //init brush
        brush = GameObject.Instantiate(Resources.Load("mesh"), controller.position, Quaternion.identity) as GameObject;
        renderer = brush.GetComponent<Renderer>();
        renderer.material.color = brushColor;
        renderer.material.SetColor("_EmissionColor", brushColor);

        strokes.Add(brush);

        //init generator imnstance
        meshGenerator = new MeshGenerator(brush.GetComponent<MeshFilter>(), controller, brush.transform);
        
    }
    public static void Doispose()
    {
        foreach (var item in strokes)
        {
            GameObject.Destroy(item);
        }
        strokes.Clear();
    }
    public static void Draw()
    {
        meshGenerator.Draw();
    }
    
}
